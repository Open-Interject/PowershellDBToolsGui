# Powershell DB Tools Gui

A program for scripting out databases and executing SQL scripts on databases.

## Getting Started

First download the executable for the program here : 
<br />  https://gitlab.com/Open-Interject/PowershellDBToolsGui/blob/master/PoShDbToolGUI/bin/Release/DbTools.exe 
<br />
and run it.


## Script Out DB tab
The Script Out tab is for generating SQL create scripts from a given database. Tables, views, stored procedures, etc. can all be scripted out.
When running the program, under the script out DB tab, you will be presented with the following input fields

### Input Fields
|Name | Example Input | Description |
|------------|-------------|---------|
|Server Name |test-server |name of the server you are trying to connect to |
|Database Name |AdventureWorks |name of the database you want to create scripts for |
|Folder Output Location |C:\Users\AdamSmith | local location to output the scripts |
|User Name|AdamSmith |Username for logging into db (if needed) |
|Password|1234supersecret |Password for logging into db (if needed) |
|Last Modified Date|1/31/2019 |Only script out objects that were last modified on this date. (blank defaults to all) |
|Specific Object Types to Script Out|Schema, Stored Procedure, Table|Types of objects you are wanting to generate scripts for (blank defaults to all) |
|Specific Objects to Script Out|[dbo].[name_table], [users].[location]|name of specific objects you are wanting to script out (blank defaults to all) |

### Options
|Name | Description |
|------------|---------|
|Delete Root Folder |When the database is originally scripted out the files are output into a folder named after that database, this option deletes that folder and outputs directly to the specified location instead |
|Use Schema Folders |creates subfolders for scripts to be stored in based on objects schema |
|Use Square Brackets |Use square brackets in object names |
|Log Output To File |If checked the program will output its log to a file |
|Do Object Search | Once enabled, will immediatelly connect to the database and list all of the objects in it, allowing the user to select specific objects to script out |

Once all your input fields and options have been customized simply select **Script Out DB** to began generating your scripts. Program logging and ouput will be shown in the Script Output box.

## Execute SQL Scripts tab
The Execute SQL Scripts tab is for executing new/altered scripts, determind by git commit differences, on a single database or multiple database(s).
When running the program, under Execute SQL Scripts tab, you will be presented with the following input fields
### Input Fields
|Name | Example Input | Description |
|------------|-------------|---------|
|Repo Folder Location |C:\GitHub\database-repo |Location of the Git repo for the database you are wanting to execute scripts from |
|Commit #1 and #2 |ca82a6dff817ec66f44342007202690a93763949 |The two commits you are comparing to generate a list of changed files  |
|Affected Files |(checkboxes) | After inputing Commit #1 and #2 select the **Get List Of Affected Files** button to view and subsaquently check which scripts you are wanting to execute |
|Server Name |test-server |name of the server you are trying to connect to |
|Database Name(s) | AdventureWorks, SandboxDB, ProductionDB |name of the database(s) you want to execute the affected files on |
|User Name|AdamSmith |Username for logging into db (if needed) |
|Password|1234supersecret |Password for logging into db (if needed) |

Once all your input customizations have been made and your affected files have been selected click **Execute SQL Changes** to execute the scripts on the specified databases. Program logging and output will be shown in the Script Output box.

## Authors

* **Adam Rodriguez**  
* **Eugene Lyubar**
* **Igor Tsymbalyuk**


