﻿using System;
using System.Xml.Linq;

namespace ExtensionMethods
{
    public static class XElementExtensions
    {
        public static string GetElementX(this XElement settingsXML, string Element, string DefaultValue)
        {
            if (settingsXML.Element(Element) == null)
            {
                return DefaultValue;
            }
            else
            {
                return settingsXML.Element(Element).Value;
            }
        }
    }

}
