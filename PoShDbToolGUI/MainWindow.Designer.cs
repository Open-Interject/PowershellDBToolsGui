﻿namespace PoShDbToolGui
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageScriptOut = new System.Windows.Forms.TabPage();
            this.checkedListBoxFolders = new System.Windows.Forms.CheckedListBox();
            this.checkedListBoxObjects = new System.Windows.Forms.CheckedListBox();
            this.buttonExecute = new System.Windows.Forms.Button();
            this.groupBoxServerInfo = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textLastModifiedDate = new System.Windows.Forms.TextBox();
            this.lblLastModifiedDate = new System.Windows.Forms.Label();
            this.textBoxOutputFolder = new System.Windows.Forms.TextBox();
            this.labelServer = new System.Windows.Forms.Label();
            this.labelDb = new System.Windows.Forms.Label();
            this.labelOutputFolder = new System.Windows.Forms.Label();
            this.textBoxServer = new System.Windows.Forms.TextBox();
            this.textBoxDb = new System.Windows.Forms.TextBox();
            this.buttonOutputFolder = new System.Windows.Forms.Button();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.labelPassword = new System.Windows.Forms.Label();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.labelUserName = new System.Windows.Forms.Label();
            this.groupBoxOptions = new System.Windows.Forms.GroupBox();
            this.labelDropCreate = new System.Windows.Forms.Label();
            this.checkBoxDropCreate = new System.Windows.Forms.CheckBox();
            this.labelObjectSearch = new System.Windows.Forms.Label();
            this.checkBoxObjectSearch = new System.Windows.Forms.CheckBox();
            this.labelLogging = new System.Windows.Forms.Label();
            this.checkBoxLogging = new System.Windows.Forms.CheckBox();
            this.labelBrackets = new System.Windows.Forms.Label();
            this.labelDeleteRoot = new System.Windows.Forms.Label();
            this.labelSchemaFolder = new System.Windows.Forms.Label();
            this.checkBoxDeleteRoot = new System.Windows.Forms.CheckBox();
            this.checkBoxSchemaFolder = new System.Windows.Forms.CheckBox();
            this.checkBoxBrackets = new System.Windows.Forms.CheckBox();
            this.buttonBrowseFolders = new System.Windows.Forms.Button();
            this.textBoxFolders = new System.Windows.Forms.TextBox();
            this.textBoxObjects = new System.Windows.Forms.TextBox();
            this.labelPsOutput = new System.Windows.Forms.Label();
            this.textBoxOutput = new System.Windows.Forms.TextBox();
            this.labelFolders = new System.Windows.Forms.Label();
            this.labelObjects = new System.Windows.Forms.Label();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.buttonBrowseObjects = new System.Windows.Forms.Button();
            this.tabPageExecSql = new System.Windows.Forms.TabPage();
            this.buttonExecuteSqlChanges = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.textBoxExecuteOutput = new System.Windows.Forms.TextBox();
            this.groupBoxGit = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.checkedListBoxAffectedFiles = new System.Windows.Forms.CheckedListBox();
            this.buttonGetAffectedFiles = new System.Windows.Forms.Button();
            this.textBoxGitCommit2 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxGitCommit1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxRepoFolder = new System.Windows.Forms.TextBox();
            this.buttonBrowseRepo = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBoxServer = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxServerName = new System.Windows.Forms.TextBox();
            this.textBoxDatabaseNames = new System.Windows.Forms.TextBox();
            this.textBoxPwd = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxUser = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLoadProfile = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSaveProfile = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl.SuspendLayout();
            this.tabPageScriptOut.SuspendLayout();
            this.groupBoxServerInfo.SuspendLayout();
            this.groupBoxOptions.SuspendLayout();
            this.tabPageExecSql.SuspendLayout();
            this.groupBoxGit.SuspendLayout();
            this.groupBoxServer.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageScriptOut);
            this.tabControl.Controls.Add(this.tabPageExecSql);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl.Location = new System.Drawing.Point(0, 22);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(759, 804);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageScriptOut
            // 
            this.tabPageScriptOut.Controls.Add(this.checkedListBoxFolders);
            this.tabPageScriptOut.Controls.Add(this.checkedListBoxObjects);
            this.tabPageScriptOut.Controls.Add(this.buttonExecute);
            this.tabPageScriptOut.Controls.Add(this.groupBoxServerInfo);
            this.tabPageScriptOut.Controls.Add(this.groupBoxOptions);
            this.tabPageScriptOut.Controls.Add(this.buttonBrowseFolders);
            this.tabPageScriptOut.Controls.Add(this.textBoxFolders);
            this.tabPageScriptOut.Controls.Add(this.textBoxObjects);
            this.tabPageScriptOut.Controls.Add(this.labelPsOutput);
            this.tabPageScriptOut.Controls.Add(this.textBoxOutput);
            this.tabPageScriptOut.Controls.Add(this.labelFolders);
            this.tabPageScriptOut.Controls.Add(this.labelObjects);
            this.tabPageScriptOut.Controls.Add(this.buttonCancel);
            this.tabPageScriptOut.Controls.Add(this.buttonBrowseObjects);
            this.tabPageScriptOut.Location = new System.Drawing.Point(4, 22);
            this.tabPageScriptOut.Name = "tabPageScriptOut";
            this.tabPageScriptOut.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageScriptOut.Size = new System.Drawing.Size(751, 778);
            this.tabPageScriptOut.TabIndex = 0;
            this.tabPageScriptOut.Text = "Script Out DB";
            this.tabPageScriptOut.UseVisualStyleBackColor = true;
            // 
            // checkedListBoxFolders
            // 
            this.checkedListBoxFolders.CheckOnClick = true;
            this.checkedListBoxFolders.FormattingEnabled = true;
            this.checkedListBoxFolders.Items.AddRange(new object[] {
            "Role",
            "Rule",
            "Schema",
            "StoredProcedure",
            "Table",
            "User",
            "UserDefinedFunction",
            "View"});
            this.checkedListBoxFolders.Location = new System.Drawing.Point(535, 281);
            this.checkedListBoxFolders.Name = "checkedListBoxFolders";
            this.checkedListBoxFolders.Size = new System.Drawing.Size(139, 124);
            this.checkedListBoxFolders.TabIndex = 25;
            this.checkedListBoxFolders.Visible = false;
            // 
            // checkedListBoxObjects
            // 
            this.checkedListBoxObjects.CheckOnClick = true;
            this.checkedListBoxObjects.FormattingEnabled = true;
            this.checkedListBoxObjects.Location = new System.Drawing.Point(279, 334);
            this.checkedListBoxObjects.MaximumSize = new System.Drawing.Size(500, 300);
            this.checkedListBoxObjects.Name = "checkedListBoxObjects";
            this.checkedListBoxObjects.Size = new System.Drawing.Size(396, 169);
            this.checkedListBoxObjects.TabIndex = 29;
            this.checkedListBoxObjects.Visible = false;
            // 
            // buttonExecute
            // 
            this.buttonExecute.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonExecute.Location = new System.Drawing.Point(656, 374);
            this.buttonExecute.Name = "buttonExecute";
            this.buttonExecute.Size = new System.Drawing.Size(87, 23);
            this.buttonExecute.TabIndex = 23;
            this.buttonExecute.Text = "Script Out DB";
            this.buttonExecute.UseVisualStyleBackColor = true;
            this.buttonExecute.Click += new System.EventHandler(this.ButtonExecute_Click);
            // 
            // groupBoxServerInfo
            // 
            this.groupBoxServerInfo.Controls.Add(this.label1);
            this.groupBoxServerInfo.Controls.Add(this.textLastModifiedDate);
            this.groupBoxServerInfo.Controls.Add(this.lblLastModifiedDate);
            this.groupBoxServerInfo.Controls.Add(this.textBoxOutputFolder);
            this.groupBoxServerInfo.Controls.Add(this.labelServer);
            this.groupBoxServerInfo.Controls.Add(this.labelDb);
            this.groupBoxServerInfo.Controls.Add(this.labelOutputFolder);
            this.groupBoxServerInfo.Controls.Add(this.textBoxServer);
            this.groupBoxServerInfo.Controls.Add(this.textBoxDb);
            this.groupBoxServerInfo.Controls.Add(this.buttonOutputFolder);
            this.groupBoxServerInfo.Controls.Add(this.textBoxPassword);
            this.groupBoxServerInfo.Controls.Add(this.labelPassword);
            this.groupBoxServerInfo.Controls.Add(this.textBoxUsername);
            this.groupBoxServerInfo.Controls.Add(this.labelUserName);
            this.groupBoxServerInfo.Location = new System.Drawing.Point(8, 6);
            this.groupBoxServerInfo.Name = "groupBoxServerInfo";
            this.groupBoxServerInfo.Size = new System.Drawing.Size(735, 221);
            this.groupBoxServerInfo.TabIndex = 27;
            this.groupBoxServerInfo.TabStop = false;
            this.groupBoxServerInfo.Text = "Server/Database";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Location = new System.Drawing.Point(307, 187);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 13);
            this.label1.TabIndex = 23;
            this.label1.Text = "MM/DD/YYYY";
            // 
            // textLastModifiedDate
            // 
            this.textLastModifiedDate.Location = new System.Drawing.Point(131, 184);
            this.textLastModifiedDate.Name = "textLastModifiedDate";
            this.textLastModifiedDate.Size = new System.Drawing.Size(174, 20);
            this.textLastModifiedDate.TabIndex = 22;
            // 
            // lblLastModifiedDate
            // 
            this.lblLastModifiedDate.AutoSize = true;
            this.lblLastModifiedDate.Location = new System.Drawing.Point(25, 187);
            this.lblLastModifiedDate.Name = "lblLastModifiedDate";
            this.lblLastModifiedDate.Size = new System.Drawing.Size(99, 13);
            this.lblLastModifiedDate.TabIndex = 21;
            this.lblLastModifiedDate.Text = "Last Modified Date:";
            // 
            // textBoxOutputFolder
            // 
            this.textBoxOutputFolder.Location = new System.Drawing.Point(130, 82);
            this.textBoxOutputFolder.Name = "textBoxOutputFolder";
            this.textBoxOutputFolder.Size = new System.Drawing.Size(565, 20);
            this.textBoxOutputFolder.TabIndex = 14;
            // 
            // labelServer
            // 
            this.labelServer.AutoSize = true;
            this.labelServer.Location = new System.Drawing.Point(46, 30);
            this.labelServer.Name = "labelServer";
            this.labelServer.Size = new System.Drawing.Size(79, 13);
            this.labelServer.TabIndex = 0;
            this.labelServer.Text = "* Server Name:";
            // 
            // labelDb
            // 
            this.labelDb.AutoSize = true;
            this.labelDb.Location = new System.Drawing.Point(31, 56);
            this.labelDb.Name = "labelDb";
            this.labelDb.Size = new System.Drawing.Size(94, 13);
            this.labelDb.TabIndex = 1;
            this.labelDb.Text = "* Database Name:";
            // 
            // labelOutputFolder
            // 
            this.labelOutputFolder.AutoSize = true;
            this.labelOutputFolder.Location = new System.Drawing.Point(0, 86);
            this.labelOutputFolder.Name = "labelOutputFolder";
            this.labelOutputFolder.Size = new System.Drawing.Size(125, 13);
            this.labelOutputFolder.TabIndex = 2;
            this.labelOutputFolder.Text = "* Folder Output Location:";
            // 
            // textBoxServer
            // 
            this.textBoxServer.Location = new System.Drawing.Point(130, 27);
            this.textBoxServer.Name = "textBoxServer";
            this.textBoxServer.Size = new System.Drawing.Size(346, 20);
            this.textBoxServer.TabIndex = 12;
            // 
            // textBoxDb
            // 
            this.textBoxDb.Location = new System.Drawing.Point(130, 53);
            this.textBoxDb.Name = "textBoxDb";
            this.textBoxDb.Size = new System.Drawing.Size(346, 20);
            this.textBoxDb.TabIndex = 13;
            // 
            // buttonOutputFolder
            // 
            this.buttonOutputFolder.Location = new System.Drawing.Point(701, 82);
            this.buttonOutputFolder.Name = "buttonOutputFolder";
            this.buttonOutputFolder.Size = new System.Drawing.Size(25, 20);
            this.buttonOutputFolder.TabIndex = 15;
            this.buttonOutputFolder.Text = "...";
            this.buttonOutputFolder.UseVisualStyleBackColor = true;
            this.buttonOutputFolder.Click += new System.EventHandler(this.ButtonOutputFolder_Click);
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(131, 138);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.Size = new System.Drawing.Size(174, 20);
            this.textBoxPassword.TabIndex = 20;
            this.textBoxPassword.UseSystemPasswordChar = true;
            // 
            // labelPassword
            // 
            this.labelPassword.AutoSize = true;
            this.labelPassword.Location = new System.Drawing.Point(69, 141);
            this.labelPassword.Name = "labelPassword";
            this.labelPassword.Size = new System.Drawing.Size(56, 13);
            this.labelPassword.TabIndex = 7;
            this.labelPassword.Text = "Password:";
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(131, 112);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(174, 20);
            this.textBoxUsername.TabIndex = 19;
            // 
            // labelUserName
            // 
            this.labelUserName.AutoSize = true;
            this.labelUserName.Location = new System.Drawing.Point(62, 115);
            this.labelUserName.Name = "labelUserName";
            this.labelUserName.Size = new System.Drawing.Size(63, 13);
            this.labelUserName.TabIndex = 6;
            this.labelUserName.Text = "User Name:";
            // 
            // groupBoxOptions
            // 
            this.groupBoxOptions.Controls.Add(this.labelDropCreate);
            this.groupBoxOptions.Controls.Add(this.checkBoxDropCreate);
            this.groupBoxOptions.Controls.Add(this.labelObjectSearch);
            this.groupBoxOptions.Controls.Add(this.checkBoxObjectSearch);
            this.groupBoxOptions.Controls.Add(this.labelLogging);
            this.groupBoxOptions.Controls.Add(this.checkBoxLogging);
            this.groupBoxOptions.Controls.Add(this.labelBrackets);
            this.groupBoxOptions.Controls.Add(this.labelDeleteRoot);
            this.groupBoxOptions.Controls.Add(this.labelSchemaFolder);
            this.groupBoxOptions.Controls.Add(this.checkBoxDeleteRoot);
            this.groupBoxOptions.Controls.Add(this.checkBoxSchemaFolder);
            this.groupBoxOptions.Controls.Add(this.checkBoxBrackets);
            this.groupBoxOptions.Location = new System.Drawing.Point(8, 233);
            this.groupBoxOptions.Name = "groupBoxOptions";
            this.groupBoxOptions.Size = new System.Drawing.Size(175, 145);
            this.groupBoxOptions.TabIndex = 26;
            this.groupBoxOptions.TabStop = false;
            this.groupBoxOptions.Text = "Options";
            // 
            // labelDropCreate
            // 
            this.labelDropCreate.AutoSize = true;
            this.labelDropCreate.Location = new System.Drawing.Point(36, 124);
            this.labelDropCreate.Name = "labelDropCreate";
            this.labelDropCreate.Size = new System.Drawing.Size(88, 13);
            this.labelDropCreate.TabIndex = 24;
            this.labelDropCreate.Text = "Drop and Create:";
            // 
            // checkBoxDropCreate
            // 
            this.checkBoxDropCreate.AutoSize = true;
            this.checkBoxDropCreate.Location = new System.Drawing.Point(130, 124);
            this.checkBoxDropCreate.Name = "checkBoxDropCreate";
            this.checkBoxDropCreate.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDropCreate.TabIndex = 23;
            this.checkBoxDropCreate.UseVisualStyleBackColor = true;
            this.checkBoxDropCreate.CheckedChanged += new System.EventHandler(this.checkBoxDropCreate_CheckedChanged);
            // 
            // labelObjectSearch
            // 
            this.labelObjectSearch.AutoSize = true;
            this.labelObjectSearch.Location = new System.Drawing.Point(29, 103);
            this.labelObjectSearch.Name = "labelObjectSearch";
            this.labelObjectSearch.Size = new System.Drawing.Size(95, 13);
            this.labelObjectSearch.TabIndex = 21;
            this.labelObjectSearch.Text = "Do Object Search:";
            // 
            // checkBoxObjectSearch
            // 
            this.checkBoxObjectSearch.AutoSize = true;
            this.checkBoxObjectSearch.Location = new System.Drawing.Point(130, 104);
            this.checkBoxObjectSearch.Name = "checkBoxObjectSearch";
            this.checkBoxObjectSearch.Size = new System.Drawing.Size(15, 14);
            this.checkBoxObjectSearch.TabIndex = 22;
            this.checkBoxObjectSearch.UseVisualStyleBackColor = true;
            this.checkBoxObjectSearch.CheckedChanged += new System.EventHandler(this.CheckBoxObjectSearch_CheckedChanged);
            // 
            // labelLogging
            // 
            this.labelLogging.AutoSize = true;
            this.labelLogging.Location = new System.Drawing.Point(26, 83);
            this.labelLogging.Name = "labelLogging";
            this.labelLogging.Size = new System.Drawing.Size(98, 13);
            this.labelLogging.TabIndex = 19;
            this.labelLogging.Text = "Log Output To File:";
            // 
            // checkBoxLogging
            // 
            this.checkBoxLogging.AutoSize = true;
            this.checkBoxLogging.Location = new System.Drawing.Point(130, 84);
            this.checkBoxLogging.Name = "checkBoxLogging";
            this.checkBoxLogging.Size = new System.Drawing.Size(15, 14);
            this.checkBoxLogging.TabIndex = 20;
            this.checkBoxLogging.UseVisualStyleBackColor = true;
            this.checkBoxLogging.CheckedChanged += new System.EventHandler(this.CheckBoxLogging_CheckedChanged);
            // 
            // labelBrackets
            // 
            this.labelBrackets.AutoSize = true;
            this.labelBrackets.Location = new System.Drawing.Point(13, 63);
            this.labelBrackets.Name = "labelBrackets";
            this.labelBrackets.Size = new System.Drawing.Size(111, 13);
            this.labelBrackets.TabIndex = 5;
            this.labelBrackets.Text = "Use Square Brackets:";
            // 
            // labelDeleteRoot
            // 
            this.labelDeleteRoot.AutoSize = true;
            this.labelDeleteRoot.Location = new System.Drawing.Point(25, 23);
            this.labelDeleteRoot.Name = "labelDeleteRoot";
            this.labelDeleteRoot.Size = new System.Drawing.Size(99, 13);
            this.labelDeleteRoot.TabIndex = 3;
            this.labelDeleteRoot.Text = "Delete Root Folder:";
            // 
            // labelSchemaFolder
            // 
            this.labelSchemaFolder.AutoSize = true;
            this.labelSchemaFolder.Location = new System.Drawing.Point(16, 43);
            this.labelSchemaFolder.Name = "labelSchemaFolder";
            this.labelSchemaFolder.Size = new System.Drawing.Size(108, 13);
            this.labelSchemaFolder.TabIndex = 4;
            this.labelSchemaFolder.Text = "Use Schema Folders:";
            // 
            // checkBoxDeleteRoot
            // 
            this.checkBoxDeleteRoot.AutoSize = true;
            this.checkBoxDeleteRoot.Location = new System.Drawing.Point(130, 24);
            this.checkBoxDeleteRoot.Name = "checkBoxDeleteRoot";
            this.checkBoxDeleteRoot.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.checkBoxDeleteRoot.Size = new System.Drawing.Size(15, 14);
            this.checkBoxDeleteRoot.TabIndex = 16;
            this.checkBoxDeleteRoot.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkBoxDeleteRoot.UseVisualStyleBackColor = true;
            this.checkBoxDeleteRoot.CheckedChanged += new System.EventHandler(this.CheckBoxDeleteRoot_CheckedChanged);
            // 
            // checkBoxSchemaFolder
            // 
            this.checkBoxSchemaFolder.AutoSize = true;
            this.checkBoxSchemaFolder.Location = new System.Drawing.Point(130, 44);
            this.checkBoxSchemaFolder.Name = "checkBoxSchemaFolder";
            this.checkBoxSchemaFolder.Size = new System.Drawing.Size(15, 14);
            this.checkBoxSchemaFolder.TabIndex = 17;
            this.checkBoxSchemaFolder.UseVisualStyleBackColor = true;
            this.checkBoxSchemaFolder.CheckedChanged += new System.EventHandler(this.CheckBoxSchemaFolder_CheckedChanged);
            // 
            // checkBoxBrackets
            // 
            this.checkBoxBrackets.AutoSize = true;
            this.checkBoxBrackets.Location = new System.Drawing.Point(130, 64);
            this.checkBoxBrackets.Name = "checkBoxBrackets";
            this.checkBoxBrackets.Size = new System.Drawing.Size(15, 14);
            this.checkBoxBrackets.TabIndex = 18;
            this.checkBoxBrackets.UseVisualStyleBackColor = true;
            this.checkBoxBrackets.CheckedChanged += new System.EventHandler(this.CheckBoxBrackets_CheckedChanged);
            // 
            // buttonBrowseFolders
            // 
            this.buttonBrowseFolders.AutoSize = true;
            this.buttonBrowseFolders.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonBrowseFolders.Location = new System.Drawing.Point(677, 254);
            this.buttonBrowseFolders.Name = "buttonBrowseFolders";
            this.buttonBrowseFolders.Size = new System.Drawing.Size(26, 23);
            this.buttonBrowseFolders.TabIndex = 24;
            this.buttonBrowseFolders.Text = "...";
            this.buttonBrowseFolders.UseVisualStyleBackColor = true;
            this.buttonBrowseFolders.Click += new System.EventHandler(this.ButtonBrowseFolders_Click);
            // 
            // textBoxFolders
            // 
            this.textBoxFolders.Location = new System.Drawing.Point(201, 255);
            this.textBoxFolders.Name = "textBoxFolders";
            this.textBoxFolders.Size = new System.Drawing.Size(473, 20);
            this.textBoxFolders.TabIndex = 22;
            // 
            // textBoxObjects
            // 
            this.textBoxObjects.Location = new System.Drawing.Point(201, 308);
            this.textBoxObjects.Name = "textBoxObjects";
            this.textBoxObjects.Size = new System.Drawing.Size(473, 20);
            this.textBoxObjects.TabIndex = 21;
            // 
            // labelPsOutput
            // 
            this.labelPsOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.labelPsOutput.AutoSize = true;
            this.labelPsOutput.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPsOutput.Location = new System.Drawing.Point(5, 381);
            this.labelPsOutput.Name = "labelPsOutput";
            this.labelPsOutput.Size = new System.Drawing.Size(93, 16);
            this.labelPsOutput.TabIndex = 11;
            this.labelPsOutput.Text = "Script Output";
            // 
            // textBoxOutput
            // 
            this.textBoxOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxOutput.Location = new System.Drawing.Point(8, 400);
            this.textBoxOutput.Multiline = true;
            this.textBoxOutput.Name = "textBoxOutput";
            this.textBoxOutput.ReadOnly = true;
            this.textBoxOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxOutput.Size = new System.Drawing.Size(735, 372);
            this.textBoxOutput.TabIndex = 10;
            // 
            // labelFolders
            // 
            this.labelFolders.AutoSize = true;
            this.labelFolders.Location = new System.Drawing.Point(200, 241);
            this.labelFolders.Name = "labelFolders";
            this.labelFolders.Size = new System.Drawing.Size(268, 13);
            this.labelFolders.TabIndex = 9;
            this.labelFolders.Text = "Specific Object Types to Script Out (leave blank for all):";
            // 
            // labelObjects
            // 
            this.labelObjects.AutoSize = true;
            this.labelObjects.Location = new System.Drawing.Point(200, 293);
            this.labelObjects.Name = "labelObjects";
            this.labelObjects.Size = new System.Drawing.Size(241, 13);
            this.labelObjects.TabIndex = 8;
            this.labelObjects.Text = "Specific Objects to Script Out (leave blank for all):";
            // 
            // buttonCancel
            // 
            this.buttonCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCancel.Enabled = false;
            this.buttonCancel.Location = new System.Drawing.Point(599, 374);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(51, 23);
            this.buttonCancel.TabIndex = 30;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.ButtonCancel_Click);
            // 
            // buttonBrowseObjects
            // 
            this.buttonBrowseObjects.AutoSize = true;
            this.buttonBrowseObjects.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.buttonBrowseObjects.Location = new System.Drawing.Point(677, 306);
            this.buttonBrowseObjects.Name = "buttonBrowseObjects";
            this.buttonBrowseObjects.Size = new System.Drawing.Size(26, 23);
            this.buttonBrowseObjects.TabIndex = 28;
            this.buttonBrowseObjects.Text = "...";
            this.buttonBrowseObjects.UseVisualStyleBackColor = true;
            this.buttonBrowseObjects.Click += new System.EventHandler(this.ButtonBrowseObjects_Click);
            // 
            // tabPageExecSql
            // 
            this.tabPageExecSql.Controls.Add(this.buttonExecuteSqlChanges);
            this.tabPageExecSql.Controls.Add(this.label11);
            this.tabPageExecSql.Controls.Add(this.textBoxExecuteOutput);
            this.tabPageExecSql.Controls.Add(this.groupBoxGit);
            this.tabPageExecSql.Controls.Add(this.groupBoxServer);
            this.tabPageExecSql.Location = new System.Drawing.Point(4, 22);
            this.tabPageExecSql.Name = "tabPageExecSql";
            this.tabPageExecSql.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageExecSql.Size = new System.Drawing.Size(751, 778);
            this.tabPageExecSql.TabIndex = 1;
            this.tabPageExecSql.Text = "Execute SQL Scripts";
            this.tabPageExecSql.UseVisualStyleBackColor = true;
            // 
            // buttonExecuteSqlChanges
            // 
            this.buttonExecuteSqlChanges.Location = new System.Drawing.Point(608, 449);
            this.buttonExecuteSqlChanges.Name = "buttonExecuteSqlChanges";
            this.buttonExecuteSqlChanges.Size = new System.Drawing.Size(135, 29);
            this.buttonExecuteSqlChanges.TabIndex = 23;
            this.buttonExecuteSqlChanges.Text = "Execute SQL Changes";
            this.buttonExecuteSqlChanges.UseVisualStyleBackColor = true;
            this.buttonExecuteSqlChanges.Click += new System.EventHandler(this.buttonExecuteSqlChanges_Click);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 462);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 16);
            this.label11.TabIndex = 31;
            this.label11.Text = "Script Output";
            // 
            // textBoxExecuteOutput
            // 
            this.textBoxExecuteOutput.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxExecuteOutput.Location = new System.Drawing.Point(8, 481);
            this.textBoxExecuteOutput.Multiline = true;
            this.textBoxExecuteOutput.Name = "textBoxExecuteOutput";
            this.textBoxExecuteOutput.ReadOnly = true;
            this.textBoxExecuteOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxExecuteOutput.Size = new System.Drawing.Size(735, 294);
            this.textBoxExecuteOutput.TabIndex = 30;
            // 
            // groupBoxGit
            // 
            this.groupBoxGit.Controls.Add(this.label9);
            this.groupBoxGit.Controls.Add(this.checkedListBoxAffectedFiles);
            this.groupBoxGit.Controls.Add(this.buttonGetAffectedFiles);
            this.groupBoxGit.Controls.Add(this.textBoxGitCommit2);
            this.groupBoxGit.Controls.Add(this.label3);
            this.groupBoxGit.Controls.Add(this.textBoxGitCommit1);
            this.groupBoxGit.Controls.Add(this.label2);
            this.groupBoxGit.Controls.Add(this.textBoxRepoFolder);
            this.groupBoxGit.Controls.Add(this.buttonBrowseRepo);
            this.groupBoxGit.Controls.Add(this.label6);
            this.groupBoxGit.Location = new System.Drawing.Point(8, 6);
            this.groupBoxGit.Name = "groupBoxGit";
            this.groupBoxGit.Size = new System.Drawing.Size(735, 295);
            this.groupBoxGit.TabIndex = 29;
            this.groupBoxGit.TabStop = false;
            this.groupBoxGit.Text = "Git Options";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(39, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(74, 13);
            this.label9.TabIndex = 22;
            this.label9.Text = "Affected Files:";
            // 
            // checkedListBoxAffectedFiles
            // 
            this.checkedListBoxAffectedFiles.FormattingEnabled = true;
            this.checkedListBoxAffectedFiles.Location = new System.Drawing.Point(120, 123);
            this.checkedListBoxAffectedFiles.Name = "checkedListBoxAffectedFiles";
            this.checkedListBoxAffectedFiles.Size = new System.Drawing.Size(565, 154);
            this.checkedListBoxAffectedFiles.TabIndex = 21;
            // 
            // buttonGetAffectedFiles
            // 
            this.buttonGetAffectedFiles.Location = new System.Drawing.Point(395, 76);
            this.buttonGetAffectedFiles.Name = "buttonGetAffectedFiles";
            this.buttonGetAffectedFiles.Size = new System.Drawing.Size(135, 28);
            this.buttonGetAffectedFiles.TabIndex = 20;
            this.buttonGetAffectedFiles.Text = "Get List Of Affected Files";
            this.buttonGetAffectedFiles.UseVisualStyleBackColor = true;
            this.buttonGetAffectedFiles.Click += new System.EventHandler(this.buttonGetAffectedFiles_Click);
            // 
            // textBoxGitCommit2
            // 
            this.textBoxGitCommit2.Location = new System.Drawing.Point(120, 84);
            this.textBoxGitCommit2.Name = "textBoxGitCommit2";
            this.textBoxGitCommit2.Size = new System.Drawing.Size(260, 20);
            this.textBoxGitCommit2.TabIndex = 19;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(53, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "Commit #2:";
            // 
            // textBoxGitCommit1
            // 
            this.textBoxGitCommit1.Location = new System.Drawing.Point(120, 58);
            this.textBoxGitCommit1.Name = "textBoxGitCommit1";
            this.textBoxGitCommit1.Size = new System.Drawing.Size(260, 20);
            this.textBoxGitCommit1.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(53, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "Commit #1:";
            // 
            // textBoxRepoFolder
            // 
            this.textBoxRepoFolder.Location = new System.Drawing.Point(120, 22);
            this.textBoxRepoFolder.Name = "textBoxRepoFolder";
            this.textBoxRepoFolder.Size = new System.Drawing.Size(565, 20);
            this.textBoxRepoFolder.TabIndex = 14;
            // 
            // buttonBrowseRepo
            // 
            this.buttonBrowseRepo.Location = new System.Drawing.Point(691, 22);
            this.buttonBrowseRepo.Name = "buttonBrowseRepo";
            this.buttonBrowseRepo.Size = new System.Drawing.Size(25, 20);
            this.buttonBrowseRepo.TabIndex = 15;
            this.buttonBrowseRepo.Text = "...";
            this.buttonBrowseRepo.UseVisualStyleBackColor = true;
            this.buttonBrowseRepo.Click += new System.EventHandler(this.buttonBrowseRepo_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(2, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(119, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "* Repo Folder Location:";
            // 
            // groupBoxServer
            // 
            this.groupBoxServer.Controls.Add(this.label10);
            this.groupBoxServer.Controls.Add(this.label4);
            this.groupBoxServer.Controls.Add(this.label5);
            this.groupBoxServer.Controls.Add(this.textBoxServerName);
            this.groupBoxServer.Controls.Add(this.textBoxDatabaseNames);
            this.groupBoxServer.Controls.Add(this.textBoxPwd);
            this.groupBoxServer.Controls.Add(this.label7);
            this.groupBoxServer.Controls.Add(this.textBoxUser);
            this.groupBoxServer.Controls.Add(this.label8);
            this.groupBoxServer.Location = new System.Drawing.Point(8, 307);
            this.groupBoxServer.Name = "groupBoxServer";
            this.groupBoxServer.Size = new System.Drawing.Size(735, 136);
            this.groupBoxServer.TabIndex = 28;
            this.groupBoxServer.TabStop = false;
            this.groupBoxServer.Text = "Server/Database";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label10.Location = new System.Drawing.Point(530, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(177, 52);
            this.label10.TabIndex = 24;
            this.label10.Text = "Comma seperated list of databases \r\nto execute changes on \r\n(leave blank to execu" +
    "te changes \r\n on database listed in Affected FIles)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(34, 35);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "* Server Name:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(98, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Database Name(s):";
            // 
            // textBoxServerName
            // 
            this.textBoxServerName.Location = new System.Drawing.Point(120, 32);
            this.textBoxServerName.Name = "textBoxServerName";
            this.textBoxServerName.Size = new System.Drawing.Size(410, 20);
            this.textBoxServerName.TabIndex = 12;
            // 
            // textBoxDatabaseNames
            // 
            this.textBoxDatabaseNames.Location = new System.Drawing.Point(120, 58);
            this.textBoxDatabaseNames.Name = "textBoxDatabaseNames";
            this.textBoxDatabaseNames.Size = new System.Drawing.Size(410, 20);
            this.textBoxDatabaseNames.TabIndex = 13;
            // 
            // textBoxPwd
            // 
            this.textBoxPwd.Location = new System.Drawing.Point(120, 111);
            this.textBoxPwd.Name = "textBoxPwd";
            this.textBoxPwd.Size = new System.Drawing.Size(174, 20);
            this.textBoxPwd.TabIndex = 20;
            this.textBoxPwd.UseSystemPasswordChar = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(57, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Password:";
            // 
            // textBoxUser
            // 
            this.textBoxUser.Location = new System.Drawing.Point(120, 85);
            this.textBoxUser.Name = "textBoxUser";
            this.textBoxUser.Size = new System.Drawing.Size(174, 20);
            this.textBoxUser.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(50, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "User Name:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(759, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLoadProfile,
            this.tsmiSaveProfile});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // tsmiLoadProfile
            // 
            this.tsmiLoadProfile.Name = "tsmiLoadProfile";
            this.tsmiLoadProfile.Size = new System.Drawing.Size(137, 22);
            this.tsmiLoadProfile.Text = "Load Profile";
            this.tsmiLoadProfile.Click += new System.EventHandler(this.tsmiLoadProfile_Click);
            // 
            // tsmiSaveProfile
            // 
            this.tsmiSaveProfile.Name = "tsmiSaveProfile";
            this.tsmiSaveProfile.Size = new System.Drawing.Size(137, 22);
            this.tsmiSaveProfile.Text = "Save Profile";
            this.tsmiSaveProfile.Click += new System.EventHandler(this.tsmiSaveProfile_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 826);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(775, 745);
            this.Name = "MainWindow";
            this.Text = "PoSh GUI";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            this.tabControl.ResumeLayout(false);
            this.tabPageScriptOut.ResumeLayout(false);
            this.tabPageScriptOut.PerformLayout();
            this.groupBoxServerInfo.ResumeLayout(false);
            this.groupBoxServerInfo.PerformLayout();
            this.groupBoxOptions.ResumeLayout(false);
            this.groupBoxOptions.PerformLayout();
            this.tabPageExecSql.ResumeLayout(false);
            this.tabPageExecSql.PerformLayout();
            this.groupBoxGit.ResumeLayout(false);
            this.groupBoxGit.PerformLayout();
            this.groupBoxServer.ResumeLayout(false);
            this.groupBoxServer.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageScriptOut;
        private System.Windows.Forms.TabPage tabPageExecSql;
        private System.Windows.Forms.Label labelServer;
        private System.Windows.Forms.Label labelFolders;
        private System.Windows.Forms.Label labelObjects;
        private System.Windows.Forms.Label labelPassword;
        private System.Windows.Forms.Label labelUserName;
        private System.Windows.Forms.Label labelBrackets;
        private System.Windows.Forms.Label labelSchemaFolder;
        private System.Windows.Forms.Label labelDeleteRoot;
        private System.Windows.Forms.Label labelOutputFolder;
        private System.Windows.Forms.Label labelDb;
        private System.Windows.Forms.Label labelPsOutput;
        private System.Windows.Forms.TextBox textBoxOutput;
        private System.Windows.Forms.TextBox textBoxFolders;
        private System.Windows.Forms.TextBox textBoxObjects;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.CheckBox checkBoxBrackets;
        private System.Windows.Forms.CheckBox checkBoxSchemaFolder;
        private System.Windows.Forms.CheckBox checkBoxDeleteRoot;
        private System.Windows.Forms.Button buttonOutputFolder;
        private System.Windows.Forms.TextBox textBoxOutputFolder;
        private System.Windows.Forms.TextBox textBoxDb;
        private System.Windows.Forms.TextBox textBoxServer;
        private System.Windows.Forms.Button buttonExecute;
        private System.Windows.Forms.CheckedListBox checkedListBoxFolders;
        private System.Windows.Forms.Button buttonBrowseFolders;
        private System.Windows.Forms.GroupBox groupBoxOptions;
        private System.Windows.Forms.Label labelLogging;
        private System.Windows.Forms.CheckBox checkBoxLogging;
        private System.Windows.Forms.GroupBox groupBoxServerInfo;
        private System.Windows.Forms.Label labelObjectSearch;
        private System.Windows.Forms.CheckBox checkBoxObjectSearch;
        private System.Windows.Forms.Button buttonBrowseObjects;
        private System.Windows.Forms.CheckedListBox checkedListBoxObjects;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textLastModifiedDate;
        private System.Windows.Forms.Label lblLastModifiedDate;
        private System.Windows.Forms.GroupBox groupBoxGit;
        private System.Windows.Forms.TextBox textBoxRepoFolder;
        private System.Windows.Forms.Button buttonBrowseRepo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBoxServer;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBoxServerName;
        private System.Windows.Forms.TextBox textBoxDatabaseNames;
        private System.Windows.Forms.TextBox textBoxPwd;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxUser;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckedListBox checkedListBoxAffectedFiles;
        private System.Windows.Forms.Button buttonGetAffectedFiles;
        private System.Windows.Forms.TextBox textBoxGitCommit2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxGitCommit1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button buttonExecuteSqlChanges;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxExecuteOutput;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tsmiLoadProfile;
        private System.Windows.Forms.ToolStripMenuItem tsmiSaveProfile;
        private System.Windows.Forms.Label labelDropCreate;
        private System.Windows.Forms.CheckBox checkBoxDropCreate;
    }
}

